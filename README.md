# Pump Finder + Selector

## Overview

This plugin provides a shortcode to display a pump finder and selector i one, `[pump_finder_selector]`, but can also be 
used individually respectively, `[pump_finder]` and `[pump_selector]`.

The basic flow of the plugin:

 - **Admin Side**:
   - In Settings, a new tab called `Pump Importer` can be found. This accepts a `.csv` file with the pump data. NOTE: 
   Only `.csv's` are accepted, Excel files will give an error.
   - The file is then parsed and the data is saved in the database. Images are included within the plugin so use the 
   naming convention from the `Market Pump Info.csv` file in the plugin.
   - The plugin will display the parsed content, indicating success. If any column is missing or blank, check for 
   hidden UniCode characters. (While making this I had &#xFEFFF a bunch for some reason)
 - **Front End**:
   - The shortcode `[pump_finder_selector]` will display the pump finder and selector in one.
     - The shortcode `[pump_finder]` will display the pump finder.
     - The shortcode `[pump_selector]` will display the pump selector.
   - The **Pump Finder** gives the user options to choose from and then gives equivalent results from the data imported earlier.
   - The **Pump Selector** gives the user options to choose from and then gives equivalent results. If no equivalent is 
   found, it will display a message saying so.

<hr>

## Bugs / Notes

 - A lot of the Pump's don't have an equivalent or an image associated. Going strictly off the csv file provided, I can't
do much about this.
 - The code is MESSY. I've done my best to clean it up, but it's hard to read. I've commented a lot of the code,
so hopefully it isn't too hard to go through. The javascript in particular is a mess because so much has changed 
in the last few days of development. It also has to be jQuery because of the jQuery-UI Widgets used.
 - The most notable 'messy' part of the code is the `get_stats_for_ps` function. Originally it has a much bigger role, 
since all the sliders on the Pump Finder would be using it. However, very recently I changed to manually doing a html
range slider with the `('[data-slider]').each(function() {});` function in `pump_selector.js` file. 
   - Following on from the javascript file mention, these could really do with being merged into one. As in the 
   `pump_selector.js` and `pump_finder.js`. This should be an easy fix, but I've only really thought about it while 
   writing this
 - Sometimes I've encountered timeouts when importing the data. I assume this is a docker issue, and it seems to have 
solved itself recently. But leaving this note just in case it reappears.
 - The html hasn't specifically been developed with mobile in mind. However, it is responsive as in the Pump Finder 
 collapses on small screens.
 - The `constructor` and `enqueue_scripts` function should really be cleaned up. With my limited knowledge of WordPress I've found a way to do what I 
 need and ran with it. I'm sure someone else could improve these a lot.

<hr>

## TODO

 - The Pump Finder could do with displaying Market matches as well as Hydron Equivalents. Currently it only shows the 
Equivalent which could get confusing when the `brand is matched with Aspen` for example. I've changed the wording to 
`brand is equivalent to` for now but, I'm unsure the correct way to go about this.
 - Merge `pump_selector.js` and `pump_finder.js` into one file.
 - Properly Depreciate the `include/hydron_pump.php` class.
 - Custom Product Types don't grab values in the product page. Not urgent, but would be nice to have.
 - In `pump_selector.js`, the url saving is broken.