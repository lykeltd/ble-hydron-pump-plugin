<?php

function wc_get_hydron_pump($post_id) {
    return new WC_Product_Hydron_Pump($post_id);
}

class WC_Product_Hydron_Pump extends WC_Product_Simple {

    public $prefix = 'lyke_';

    public static $tags = [
	    'input_voltage' => 'Input Voltage',
	    'pumping_head' => 'Pumping Head',
	    'max_flow_rate' => 'Max Flow Rate',
	    'tank_capacity' => 'Tank Capacity',
	    'noise_level' => 'Noise Level',
	    'max_cooling_capacity' => 'Max Cooling Capacity',
	    'ambient_temperature' => 'Ambient Temperature',
	    'power_consumption' => 'Power Consumption',
	    'inlet_id' => 'Inlet ID',
	    'outlet_id' => 'Outlet ID',
	    'suitable_for' => 'Suitable for',
	    'description' => 'Description'

    ];

    public function get_type()
    {
        return 'hydron_pump';
    }

    public function is_purchasable()
    {
        return true;
    }

    public function grab_stats(): array
    {
        $stats = [];

        foreach (self::$tags as $key => $value) {
            $stats[$value] = $this->get_meta($this->prefix . $key);
        }

        return $stats;
    }

    public static function get_default_stats(): array
    {
        $stats = [];

        foreach (self::$tags as $key => $value) {
            $stats[$value] = '';
        }

        return $stats;
    }

//    public function set_sku($sku)
//    {
//        $this->update_meta_data($this->prefix . 'sku', $sku);
//    }


    public function set_name( $name ) {
        $this->set_prop( 'name', $name );
        return $this;
    }

    public function set_input_voltage($input_voltage)
    {
        $this->update_meta_data($this->prefix . 'input_voltage',$input_voltage);
    }

    public function set_pumping_head($pumping_head)
    {
	    $this->update_meta_data($this->prefix . 'pumping_head',$pumping_head);
    }

    public function set_max_flow_rate($max_flow_rate)
    {
	    $this->update_meta_data($this->prefix . 'max_flow_rate',$max_flow_rate);
    }

    public function set_tank_capacity($tank_capacity)
    {
	    $this->update_meta_data($this->prefix . 'tank_capacity',$tank_capacity);
    }

    public function set_noise_level($noise_level)
    {
	    $this->update_meta_data($this->prefix . 'noise_level',$noise_level);
    }

    public function set_max_cooling_capacity($max_cooling_capacity)
    {
	    $this->update_meta_data($this->prefix . 'max_cooling_capacity',$max_cooling_capacity);
    }

    public function set_ambient_temperature($ambient_temperature)
    {
	    $this->update_meta_data($this->prefix . 'ambient_temperature',$ambient_temperature);
    }

    public function set_power_consumption($power_consumption)
    {
	    $this->update_meta_data($this->prefix . 'power_consumption',$power_consumption);
    }

    public function set_inlet_id($inlet_id)
    {
	    $this->update_meta_data($this->prefix . 'inlet_id',$inlet_id);
    }

    public function set_outlet_id($outlet_id)
    {
	    $this->update_meta_data($this->prefix . 'outlet_id',$outlet_id);
    }

    public function set_suitable_for($pumping_head)
    {
	    $this->update_meta_data($this->prefix . 'suitable_for',$pumping_head);
    }

    public function set_description($description)
    {
	    $this->update_meta_data($this->prefix . 'description',$description);
    }
}