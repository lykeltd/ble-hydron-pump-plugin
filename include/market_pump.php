<?php


class WC_Product_Market_Pump extends WC_Product_Simple {

    private $prefix = 'lyke_';

    public $tags = [
        'brand' => 'Brand',
        'max_temps' => 'Max Temps',
        'inlets_no' => 'Inlets No.',
        'connection_size' => 'Connection Size',
        'ip_rating' => 'IP Rating',
        'safety' => 'Safety',
        'thermal_protection' => 'Thermal Protection',
        'features' => 'Features',
        'suction_lift' => 'Suction Lift',
        'dimensions' => 'Dimensions',
        'height' => 'Height',
        'width' => 'Width',
        'depth' => 'Depth',
        'outlets_no' => 'Outlet No.',
        'man_part' => 'Man Part',
        'dist_part' => 'Dist Part',
        'price_cheapest_listed' => 'Cheapest Listed',
        'hydron_equiv' => 'Hydron Equivalent',
        'cost' => 'Cost',
        'flow_rate' => 'Flow Rate',
        'max_head' => 'Max Head',
        'tank_capacity' => 'Tank Capacity',
        'sound' => 'Sound',
        'power' => 'Power',
        'rated' => 'Rated',
        'class' => 'Class',
        'max_units_outputted' => 'Max Units Outputted',
	    'sku' => 'SKU',
        'image' => 'Image',
        'image_link' => 'Image Link',
    ];

    public function get_type()
    {
        return 'market_pump';
    }

    public function is_purchasable()
    {
        return false;
    }

    // Table of meta

    public function grab_stats(): array
    {
        $stats = [];

        foreach ($this->tags as $key => $value) {
            $stats[$key] = $this->get_meta($this->prefix . $key);
        }

        return $stats;
    }

    public function get_default_stats(): array
    {
        $stats = [];

        foreach ($this->tags as $key => $value) {
            $stats[$value] = '';
        }

        return $stats;
    }

    // SETTERS

    public function set_brand($val) {
        $this->update_meta_data($this->prefix . "brand", $val);
        return $this;
    }
    public function set_max_temp($val) {
        $this->update_meta_data($this->prefix . "max_temp", $val);
        return $this;
    }
    public function set_inlets_no($val)
    {
        $this->update_meta_data($this->prefix . "inlets_no", $val);
        return $this;
    }
    public function set_connection_size($val)
    {
        $this->update_meta_data($this->prefix . "connection_size", $val);
        return $this;
    }
    public function set_ip_rating($val)
    {
        $this->update_meta_data($this->prefix . "ip_rating", $val);
        return $this;
    }
    public function set_safety($val)
    {
        $this->update_meta_data($this->prefix . "safety", $val);
        return $this;
    }
    public function set_thermal_protection($val)
    {
        $this->update_meta_data($this->prefix . "thermal_protection", $val);
        return $this;
    }
    public function set_features($val)
    {
        $this->update_meta_data($this->prefix . "features", $val);
        return $this;
    }
    public function set_suction_lift($val)
    {
        $this->update_meta_data($this->prefix . "suction_lift", $val);
        return $this;
    }
    public function set_dimensions($val)
    {
        $this->update_meta_data($this->prefix . "dimensions", $val);
        return $this;
    }
    public function set_outlets_no($val)
    {
        $this->update_meta_data($this->prefix . "outlets_no", $val);
        return $this;
    }
    public function set_man_part($val)
    {
        $this->update_meta_data($this->prefix . "man_part", $val);
        return $this;
    }
    public function set_dist_part($val)
    {
        $this->update_meta_data($this->prefix . "dist_part", $val);
        return $this;
    }
    public function set_price_cheapest_listed($val)
    {
        $this->update_meta_data($this->prefix . "price_cheapest_listed", $val);
        return $this;
    }
    public function set_hydron_equiv($val)
    {
        $this->update_meta_data($this->prefix . "hydron_equiv", $val);
        return $this;
    }
    public function set_cost($val)
    {
        $this->update_meta_data($this->prefix . "cost", $val);
        return $this;
    }
    public function set_flow_rate($val)
    {
        $this->update_meta_data($this->prefix . "flow_rate", $val);
        return $this;
    }
    public function set_max_head($val)
    {
        $this->update_meta_data($this->prefix . "max_head", $val);
        return $this;
    }
    public function set_tank_capacity($val)
    {
        $this->update_meta_data($this->prefix . "tank_capacity", $val);
        return $this;
    }
    public function set_sound($val)
    {
        $this->update_meta_data($this->prefix . "sound", $val);
        return $this;
    }
    public function set_power($val)
    {
        $this->update_meta_data($this->prefix . "power", $val);
        return $this;
    }
    public function set_rated($val)
    {
        $this->update_meta_data($this->prefix . "rated", $val);
        return $this;
    }
    public function set_class($val)
    {
        $this->update_meta_data($this->prefix . "class", $val);
        return $this;
    }
    public function set_max_units_output($val)
    {
        $this->update_meta_data($this->prefix . "max_units_outputted", $val);
        return $this;
    }
    public function set_tags($tags) {
        $this->tags = $tags;
        return $this;
    }

    public function getTags() {
        return $this->tags;
    }

    public function get_equiv() {
        $equiv = $this->get_meta($this->prefix . "hydron_equiv");
        if ($equiv && wc_get_product_id_by_sku($equiv)) {
            return wc_get_product(wc_get_product_id_by_sku($equiv));
        }

        return false;
    }

    public function update_meta($key, $val): static
    {
        $this->update_meta_data($this->prefix . $key, $val);
        return $this;
    }

    public function get_full_meta() {
        $metas = $this->get_meta_data();
        $full_meta = [];

        foreach ($metas as $meta) {
            $full_meta[$meta->key] = $meta->value;
        }

        return $full_meta;
    }

//	public function set_sku($val)
//    {
//        return $this->update_meta_data($this->prefix . "sku", $val);
//    }
}
