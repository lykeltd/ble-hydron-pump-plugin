<?php

/**
 * Plugin Name:         Pump Finder and Viewer
 * Description:         Adds shortcodes for finding and viewing pumps
 * Version:             1.0.0
 * Author:              Lyke
 * Author URI:
 * Requires PHP:        7.3
 * Requires at least:   5.8
 */

class lyke_pumper {

	function __construct()
	{
		add_action( 'wp_enqueue_scripts', [$this, 'enqueue_scripts'] );

		add_shortcode('pump_finder', [$this, 'pump_finder'] );
		add_shortcode('pump_selector', [$this, 'pump_selector'] );
		add_shortcode('pump_finder_selector', [$this, 'pumps'] );

		add_action( 'wp_ajax_pf_setup_selects', [$this, 'pf_setup_selects'] );
		add_action( 'wp_ajax_pf_search', [$this, 'pf_search'] );
		add_action( 'wp_ajax_ps_search', [$this, 'ps_search'] );

        // Importer
        add_action( 'admin_menu', [$this, 'register_custom_page'] );
        add_action( 'admin_enqueue_scripts', [$this, 'admin_enqueue'] );

        add_action('init', [$this, 'init']);
        add_action("woocommerce_loaded", [$this, "init_woo"]);

        // Adding the pump product types
        add_filter( 'product_type_selector', [$this, 'custom_product_types'] );
        add_filter( 'woocommerce_product_data_tabs', [$this, 'pump_tabs'] );
        add_action( 'woocommerce_product_data_panels', [$this, 'pump_tab_content'], 1, 1 );

	}

    /**
     *
     * Displays the pump finder and selector in one for the shortcode
     *
     * @return void
     */
    function pumps(): void
    {
        ?>
            <div class="pump-container">
                <div class="nav">
                    <span class="nav-item active" id="pump-selector-tab" data-target="selector">Pump Selector</span>
                    <span class="nav-item" id="pump-finder-tab" data-target="finder">Pump Finder</span>
                </div>
                <div class="pump-display">
                    <div id="selector"><?php echo do_shortcode('[lyke_pump_selector]') ?></div>
                    <div id="finder" class="hide"><?php echo do_shortcode('[lyke_pump_finder]') ?></div>
                </div>
            </div>
        <?php
    }

    /**
     * Attempts to register custom product types
     *
     * @param $types
     * @return mixed
     */
    function custom_product_types($types ): mixed
    {
        $types['hydron_pump'] = __('Hydron Pump', 'hydron_pump'); // Depreciate this
        $types['market_pump'] = __('Market Pump', 'market_pump');

        return $types;
    }

    /**
     * Adds the pump tabs to the product data
     *
     * @param $tabs
     * @return mixed
     */
    function pump_tabs($tabs ): mixed
    {
        $tabs['Hydron Pump stats'] = array(
                'label' => __('Hydron Pump Stats', 'hydron_pump_stats'),
                'target' => 'hydron_pump_options',
                'class' => array( 'hide_if_simple', 'hide_if_market_pump', 'show_if_hydron_pump' )
        );

        $tabs['Market Pump stats'] = array(
                'label' => __('Market Pump Stats', 'market_pump_stats'),
                'target' => 'market_pump_options',
                'class' => array( 'hide_if_simple', 'hide_if_hydron_pump', 'show_if_market_pump' )
        );

        return $tabs;
    }

    /**
     * Fills in the values for the product type section
     *
     * @return void
     */
    function pump_tab_content(): void
    {
        // This doesn't work as intended. Should be grabbing data from the meta, but it's old code. This function needs rewriting.

        ?><div id='hydron_pump_options' class='panel woocommerce_options_panel'><?php
        ?><div class='options_group'><?php

        $product = wc_get_product(get_the_ID());

        /* @var WC_Product_Hydron_Pump $product */
        $stats = $product instanceof WC_Product_Hydron_Pump ? $product->grab_stats() : WC_Product_Hydron_Pump::get_default_stats();

        foreach ( $product->tags as $key=>$stat ) {
            woocommerce_wp_text_input(
                array(
                    'id' => 'hydron_stat[' . $key . ']',
                    'label' => __( $stat, 'pump_stat' ),
                    'placeholder' => $stats[$key],
                    'desc_tip' => 'true',
                    'description' => __( '.', 'dm_product' ),
                    'type' => 'text'
                )
            );
        }
        ?>

        <?php

        ?></div>
        </div><?php


        ?><div id='market_pump_options' class='panel woocommerce_options_panel'><?php
        ?><div class='options_group'><?php

        $product = wc_get_product(get_the_ID());


        /* @var WC_Product_Market_Pump $product */
        $stats = $product instanceof WC_Product_Market_Pump ? $product->grab_stats() : WC_Product_Market_Pump::get_default_stats();

        foreach ( $stats as $key=>$stat ) {
            woocommerce_wp_text_input(
                array(
                    'id' => $key . '_market_stat',
                    'label' => __( $key, 'pump_stat' ),
                    'placeholder' => $stat,
                    'desc_tip' => 'true',
                    'description' => __( 'Enter Demo product Info.', 'dm_product' ),
                    'type' => 'text'
                )
            );
        }
        ?></div>
        </div><?php
    }

    /**
     * Parses data for the Pump Finder dropdown
     *
     * @return array
     */
    function pf_setup_selects(): array {

		$brandsAndSkus = $this->get_brands_and_skus_for_pf();

		$brandsHtml = '';

		foreach ($brandsAndSkus[0] as $brand) {
			$brandsHtml .= '<option value="brand-' . str_replace(' ', '-', $brand) . '">' . $brand . '</option>';
		}

		$skusHtml = '';

		foreach ($brandsAndSkus[1] as $sku) {
			$skusHtml .= '<option value="sku-' . str_replace(' ', '-', $sku) . '">' . $sku . '</option>';
		}

		return [$brandsHtml, $skusHtml, $brandsAndSkus];
	}

	function pf_search() {

		$query_args = [
			'limit' => -1,
			'meta_key' => 'lyke_' . $_REQUEST['data']['type'],
			'meta_value' => $_REQUEST['data']['value'],
			'tax_query' => [
				[
					'taxonomy' => 'product_type',
					'field'    => 'slug',
					'terms'    => 'market_pump',
				],
			],
		];

		if ($query_args == '') wp_send_json_error(' Incorrect search type ');

		$products    = new WC_Product_Query( $query_args );
		$products = $products->get_products();


		$resultsHtml = '';


		if (count($products) < 1) wp_send_json_error(' Incorrect Brand / Sku ');

		foreach ($products as $product) {

			$title = $product->get_sku();
            $image = $product->get_image() ?? '/';
			$hydron_equiv = $product->grab_stats()['Hydron Equivalent'];

			if ( $hydron_equiv ) {


				$equivalent_product_id = wc_get_product_id_by_sku($hydron_equiv);
				$equivalent_product = wc_get_product($equivalent_product_id);


				if ($equivalent_product) {
					// Equiv found in db
					$permalink = $equivalent_product->get_permalink();
					$equivalent_product_permalink = $permalink;
				}
				else {
					$equivalent_product_permalink = 'No equivalent';
				}
			}
			else {
				$equivalent_product_permalink = 'No equivalent';
			}

			$resultsHtml .= '
				<li class="box">
					<a href="' . ( $hydron_equiv ? $equivalent_product_permalink : "#" ) . '">
						<span>
						    ' . $image . '
							<h5>' . $title . '</h5>
							<p id="sku">' . $equivalent_product_permalink . '</p>
						</span>
					</a>
				</li>';
		}

		wp_send_json_success([$resultsHtml,  $_REQUEST]);
	}

    /**
     * Prints the dropdown for the Pump Finder. It sorts by Brand and Sku's
     *
     * @return string
     */
    function pump_finder(): string
	{
		$options = $this->pf_setup_selects();

		return '
			<div class="lyke-container" id="pf-container">
				<span class="pf-bar">
					<select id="pf-selector">
					<optgroup label = "Brands"></optgroup>
					' . $options[0] . '
					<optgroup label = "Market Pumps"></optgroup>
					' . $options[1] . '
					</select>
					
				</span>
				
				<ul class="pf-display-container" id="pf-display-container">
				</ul>
			</div>
			';
	}

    /**
     * Parses data for the Pump Finder dropdown into brand and Sku's
     *
     * @return array[]
     */
    function get_brands_and_skus_for_pf(): array {

		$query_args = [
			'nopaging' => true,
			'tax_query' => [
				[
					'taxonomy' => 'product_type',
					'field'    => 'slug',
					'terms'    => 'market_pump',
				],
			],
		];

		/** @var WC_Product[] $products */
		$products             = wc_get_products( $query_args );
		$product_types_brands = [];
		$product_types_skus   = [];

		foreach ($products as $product) {

			$brand = isset($product->get_full_meta()['lyke_brand']) ? $product->get_full_meta()['lyke_brand'] : 'No Brand';
			if (!in_array($brand, $product_types_brands) && $brand) {
				$product_types_brands[$product->get_id()] = $brand;
			}

			$sku = $product->get_title();
			if (!in_array($sku, $product_types_skus) && $sku) {
				$product_types_skus[$product->get_id()] = $sku;
			}
		}

		return [$product_types_brands, $product_types_skus];
	}

    /**
     * Preforms a search for the Pump Selector via SQL
     *
     * @return void
     */
    function ps_search(): void
    {
		$user_args = $_REQUEST['args'];

		$args = [];
		if (!$user_args) wp_send_json_error('No args');

		foreach ($user_args as $query) {
            if ($query['value'] == 'N/A') continue;
			$args[$query['id']] = $query['value'];
		}
        global $wpdb;

        $query = '
			SELECT * FROM ' . $wpdb->prefix . 'posts
			INNER JOIN ' . $wpdb->prefix . 'postmeta ON ' . $wpdb->prefix . 'posts.ID = ' . $wpdb->prefix . 'postmeta.post_id
			INNER JOIN ' . $wpdb->prefix . 'term_relationships ON ' . $wpdb->prefix . 'posts.ID = ' . $wpdb->prefix . 'term_relationships.object_id
			INNER JOIN ' . $wpdb->prefix . 'terms ON ' . $wpdb->prefix . 'term_relationships.term_taxonomy_id = ' . $wpdb->prefix . 'terms.term_id
			WHERE post_type = "product" AND post_status = "publish" AND slug = "market_pump" ';

        foreach ($args as $key => $value) {
            if (str_contains($value, '-')) {
                $query .= 'OR meta_key = "lyke_' . $key . '" AND meta_value BETWEEN ' . explode('-', $value)[0] . ' AND ' . explode('-', $value)[1];
            } else {
                $query .= 'OR meta_key = "lyke_' . $key . '" AND meta_value LIKE "' . $value . '" ';
            }

        }

		$results = $wpdb->get_results($query, ARRAY_A);


		$matches = [];
		$added_products = [];
		foreach ( $results as $product ) {

			foreach ( $args as $key => $user_stat ) {
				if ($product['meta_key'] === ('lyke_' . $key) ) {

                    /** @var WC_Product_Market_Pump $wc_product */
                    $wc_product = wc_get_product($product['ID']);
                    $equiv = $wc_product->get_equiv();

                    if (!$equiv) continue;
                    if (in_array($equiv->get_sku(), $added_products)) continue;


					$matches[] = [
						'link' => $equiv->get_permalink(),
						'id' => $equiv->get_id(),
						'sku' => $equiv->get_sku(),
                        'img' => $equiv->get_image(),
					];

					$added_products[] = $equiv->get_sku();
				}
			}
		}

        $resultsHtml = '<span>Results: <strong>' . count($matches) . '</strong> Matches with: </span> <ul>';

        foreach ($args as $key => $value) {
            if (str_contains($value, '-')) {
                $resultsHtml .= '<li>' . str_replace('_', ' ', $key) . ' between ' . explode('-', $value)[0] . ' and ' . explode('-', $value)[1] . '</li>';
            } else {
                $resultsHtml .= '<li>' . $key . ' is equivalent to ' . $value . '</li>';
            }
        }
        $resultsHtml .= '</ul>';

		$resultsHtml .= '<ul class="display-container">';

		foreach ($matches as $match) {
			$resultsHtml .= '
				<li class="box">
					<a href="' . $match['link'] . '">
						<span>
						    ' . $match['img'] . '
							<h5>' . $match['sku'] . '</h5>
							<p id="sku">' . $match['id'] . '</p>
							<div class="preview"></div>
						</span>
					</a>
				</li>';
		}
		$resultsHtml .= '</ul>';

		wp_send_json_success([$resultsHtml, $matches, $added_products]);
	}

    /**
     * Prints the sliders for the Pump Selector
     *
     * @return string
     */
    function pump_selector(): string
	{
		// Get all market pumps
		$query = [
			'post_type' => 'product',
			'nopaging' => true,
			'tax_query' => [
				[
					'taxonomy' => 'product_type',
					'field'    => 'slug',
					'terms'    => 'market_pump',
				],

			]
		];

		/** @var WC_Product_Market_Pump[] $market_pumps */
		$market_pumps = wc_get_products($query);

		// Filter which ones have a valid equivalent

		$filtered_market_pumps = array_filter($market_pumps, function($pump) {
			$equiv = $pump->grab_stats()['hydron_equiv'];
			if (!$equiv) return false;


			/** @var WC_Product_Hydron_Pump $equivalent_product */
			$equivalent_product_id = wc_get_product_id_by_sku($equiv);

			if (!$equivalent_product_id) return false;

			return true;
		});

		// Question time
		$ps_container = '
					<div class="lyke-container">
						<h1>Pump Selector</h1>
						<p>Match the specs as close as possible and we\'ll try and find the best suited pump for you!</p>
						<hr style="width: 50%">
					</div>
					<form id="ps-search-form" action="">
			';

		$stats = array_keys($this->get_stats_for_ps());

		foreach ($stats as $key) {
			$ps_container .= '
						<div class="slider-container">
							<h5>' . $key . '</h5>
							<p id="ps_' . $key . '_result" class="ps-result">N/A</p>
													
							<div class="slider-content">
								<div class="ps-slider" id="ps_' . $key . '_slider"></div>
							</div>	
						</div>';
		}

        $ps_container .= '
						<div class="slider-container">
							<h5>Inlets No.</h5>
							<p id="ps_inlets_no_result" class="ps-result">N/A</p>
													
							<div class="slider-content">
								<div class="ps-slider" id="ps_inlets_no_slider" data-mix="0" data-max="20" data-slider></div>
							</div>	
						</div>
						
						<div class="slider-container">
							<h5>Outlets No.</h5>
							<p id="ps_outlets_no_result" class="ps-result">N/A</p>
													
							<div class="slider-content">
								<div class="ps-slider" id="ps_outlets_no_slider" data-mix="0" data-max="20" data-slider></div>
							</div>	
						</div>
						
						<div class="slider-container">
							<h5>Width</h5>
							<p id="ps_width_result" class="ps-result">N/A</p>
													
							<div class="slider-content">
								<div class="ps-slider" id="ps_width_no_slider" data-mix="0" data-max="20" data-slider></div>
							</div>	
						</div>
						
						<div class="slider-container">
							<h5>Height</h5>
							<p id="ps_height_result" class="ps-result">N/A</p>
													
							<div class="slider-content">
								<div class="ps-slider" id="ps_height_slider" data-mix="0" data-max="600" data-slider></div>
							</div>	
						</div>
						
						<div class="slider-container">
							<h5>Depth</h5>
							<p id="ps_depth_result" class="ps-result">N/A</p>
													
							<div class="slider-content">
								<div class="ps-slider" id="ps_depth_slider" data-mix="0" data-max="300" data-slider></div>
							</div>	
						</div>
						
						<div class="slider-container">
							<h5>Flow Rate</h5>
							<p id="ps_flow_rate_result" class="ps-result">N/A</p>
													
							<div class="slider-content">
								<div class="ps-slider" id="ps_flow_rate_no_slider" data-mix="0" data-max="1500" data-slider></div>
							</div>	
						</div>
						';

		$ps_container .= '
						<input type="submit" id="ps_submit_btn" />
					</form>
					<div id="ps-results" class="lyke-container">
					</div>
					<button id="ps-back-btn" class="hidden">Back</button>';

		return $ps_container;
	}

    /**
     * Parses stats for Pump Selector
     *
     * @return array
     */
    function get_stats_for_ps(): array {
		$stats = [];

        $args = ['category' => array( 'market-pumps' )];

		$products = new WC_Product_Query($args);
		$products = $products->get_products();

		// Grab unique stats
		foreach ($products as $product) {

			$product_stats = $product->grab_stats();

			foreach ($product_stats as $key => $stat) {
				if (!array_key_exists($key, $stats))
					$stats[$key] = [$stat];
				else
					if (!in_array($stat, $stats[$key]) && $stat != '' && $stat != null) $stats[$key][] = $stat;
			}
		}

        $fullStats = [];

		// Sort
		foreach ($stats as $key => $value) {
            if(count($value) == 1 && $value[0] == '') continue;
            if (!in_array($key, ['brand'])) continue;

            $fullStats[$key] = array_map(function($value) {

				return ['value' => explode(' ', $value)[0], 'unit' => ''];
			}, $value);

            $fullStats[$key] = array_filter($fullStats[$key], function($value) {
                $value = strtoupper($value['value']);
                return
                    $value !== '' &&
                    $value !== 'N/A' &&
                    $value !== 'YES';
            });


            usort($fullStats[$key], function ($a,$b) {
				return $a['value'] <=> $b['value'];
			});

			$sortedList = array_values($fullStats[$key]);
            $fullStats[$key] = $sortedList;
		}

        return $fullStats;
	}

	function enqueue_scripts() {
		wp_enqueue_style('lyke-pumper-style', plugin_dir_url(__FILE__) . 'css/style.css');
		wp_enqueue_style('loading-bar-style', plugin_dir_url(__FILE__) . 'css/loading-bar.min.css');
		wp_enqueue_style('jquery-ui-style', plugin_dir_url(__FILE__) . 'css/jquery-ui.min.css');
		wp_enqueue_style('select2-style', plugin_dir_url(__FILE__) . 'css/select2.min.css');

		wp_register_script('jquery-ui', plugin_dir_url(__FILE__) . 'js/jquery-ui.min.js');
		wp_register_script('loading-bar', plugin_dir_url(__FILE__) . 'js/loading-bar.min.js');
		wp_register_script('select2', plugin_dir_url(__FILE__) . 'js/select2.full.min.js');
		wp_register_script('lyke_url_handler', plugin_dir_url(__FILE__) . 'js/lyke_url_handler.js');

		wp_register_script('lyke_pump_finder', plugin_dir_url(__FILE__) . 'js/pump_finder.js', array('jquery', 'jquery-ui', 'lyke_url_handler', 'loading-bar', 'select2'), '1.0', true );
		wp_register_script('lyke_pump_selector', plugin_dir_url(__FILE__) . 'js/pump_selector.js', array('jquery', 'jquery-ui', 'lyke_url_handler'), '1.0', true );

		wp_localize_script( 'lyke_pump_finder', 'lyke', [ 'ajaxurl' => admin_url( 'admin-ajax.php' ) ] );
		wp_localize_script( 'lyke_pump_selector', 'lyke', [ 'ajaxurl' => admin_url( 'admin-ajax.php'), 'pump_stats' => $this->get_stats_for_ps(), 'security' => wp_create_nonce( 'file_upload' ) ] );
//
//		wp_enqueue_script('lyke_pump_finder');
//		wp_enqueue_script('lyke_pump_selector');

        wp_register_script('lyke_pumper', plugin_dir_url(__FILE__) . 'js/full.js', array( 'lyke_pump_finder', 'lyke_pump_selector', 'jquery', 'jquery-ui', 'lyke_url_handler', 'loading-bar', 'select2'), '1.0', true );
        wp_enqueue_script( 'lyke_pumper' );


    }

    function admin_enqueue() {
        wp_enqueue_style('lyke-custom-importer', plugin_dir_url(__FILE__) . 'css/admin-style.css');

    }

    /**
     * WooCommerce Init
     *
     * @return void
     */
    function init(): void
    {
        if (!term_exists('Market Pump', 'product_cat')) {
            wp_insert_term(
                'Market Pump',
                'product_cat',
                array(
                    'description' => 'Pumps on the market',
                    'slug'        => 'market-pumps'
                )
            );
        }

        if (!term_exists('Hydron Pump', 'product_cat')) {
            $i = wp_insert_term(
                'Hydron Pump',
                'product_cat',
                array(
                    'description' => 'Hydron Pumps by BLE',
                    'slug'        => 'hydron-pumps'
                )
            );
        }

    }

    /**
     *
     * Registers the custom post type
     *
     * @return void
     *
     */
    function init_woo(): void
    {

        require_once "include/hydron_pump.php";
        require_once "include/market_pump.php";
    }

    /**
     *
     * Registers Custom Page
     *
     * @return void
     */
    function register_custom_page(): void
    {
        add_menu_page(
            "Pump_importer",
            "Pump Importer",
            "manage_options",
            "lyke_custom_importer",
            [$this, "importer_page"]

        );
    }

    /**
     *
     * Renders the `Admin Importer Page. It consists of 3 parts handled here via a `step` post variable
     *
     * @throws WC_Data_Exception
     */
    function importer_page(): void
    {
        $step = $_POST['step'] ?? false;

        if (!$step) {
            ?>
            <div class="form-container">
                <span class="header">Step 1 - Upload CVS</span>

                <form method="POST" name="market-pump-form" enctype="multipart/form-data">
                    <input type="file" id="market-pump-input" name="market-pump-file">
                    <input type="hidden" value="market" name="pump-type">
                    <input type="hidden" value="choose_headers" name="step">

                    <input type="submit">
                </form>
            </div>
            <?php
        } else if ($step === 'choose_headers') {
            $this->choose_headers();

        } else if ($step === 'create_products') {
            $this->create_products();

        }

        if (isset($_POST['pump-ids'])) {
            $ids = explode(',', $_POST['pump-ids']);

            foreach ($ids as $id) {
                wp_delete_post($id);
            }
        }

    }

    /**
     *
     * Manages the File Uploads (csv) and allows the user to select headers to import
     *
     * @return void
     */
    function choose_headers(): void
    {

        $temp_name = $_FILES['market-pump-file']['tmp_name'];

        $newName = uniqid('upload_', true);
        $fullPath = plugin_dir_path(__FILE__) . 'tmp/' . $newName;

        if (!is_dir(plugin_dir_path(__FILE__) . 'tmp/')) mkdir(plugin_dir_path(__FILE__) . 'tmp/');

        move_uploaded_file($temp_name, $fullPath . '.csv');

        if ($temp_name) {

            $csv = array_map('str_getcsv', file($fullPath . '.csv'));

            ?>
            <div class="form-container">
                <span class="header">Step 2 - CVS Options</span>
                <form method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="pump-type" value="market">
                    <input type="hidden" name="pump-file" value="<?php echo $fullPath . '.csv' ?>">
                    <input type="hidden" value="create_products" name="step">

                    <div class="options-container">
                        <span class="caption">Detected Headers:</span>

                        <div class="options-row-container">
                            <?php
                            for ($i = 0; $i < count($csv[0]); $i++) {
                                ?>
                                <div class="options-row">
                                    <span class="box"><?php echo $csv[0][$i] ?></span>
                                    <div class="box">
                                        <input type="checkbox" id="<?php echo 'toggle_' . $csv[0][$i] ?>" class="btn" checked name="headers[]" value="<?php echo $csv[0][$i] ?>">
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <input type="text" disabled value="Auto Detected: Market">

                    <input type="submit">
                </form>
            </div>
            <?php
        }
    }


    /**
     *
     * Handles creating the products from the csv file
     *
     * @throws WC_Data_Exception
     */
    function create_products(): void
    {

        if (!isset($_POST['headers'])) {
            ?>
                Something went wrong! (Headers not set)
            <?php
            return;
        }

        $headers = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $_POST['headers']); // Attempts to remove any non-ascii characters

        $csv = array_map('str_getcsv', file($_POST['pump-file']));

        $uploadedIds = [];

        if (!$csv) {

            echo 'Something Went Wrong! (CSV not found)';
        }

        $indexedLines = [];
        foreach ($headers as $i => $header) {
            $indexedLines[$header] = $i;
        }

        // Create entities
        foreach ($csv as $i=>$line) {
            $line = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $line); // Twice for good luck
            if ($i === 0) continue;
            $product = wc_get_product(wc_get_product_id_by_sku($line[$indexedLines['sku']]));


            // If product exists, use it. If not, create a new one. Unique SKU's are required
            if (!$product) {
                $product = new WC_Product_Market_Pump();
            }

            foreach ($indexedLines as $header=>$value) {
                $product->update_meta($header, $line[$value]);
            }

            if (isset($indexedLines['sku'])) {
                $product->set_sku($line[$indexedLines['sku']]);
                $product->set_name($line[$indexedLines['sku']]);
            }

            if (isset($indexedLines['cost'])) {
                $product->set_price($line[$indexedLines['cost']]);
            }

            if (isset($indexedLines['image'])) {
                $this->Generate_Featured_Image(plugin_dir_path(__FILE__) . 'assets/img/' . $line[$indexedLines['image']] . '.jpg', $product->get_id());
            }

            $product->set_status('publish');
            $product->set_tags($headers);

            $id = $product->save();

            $uploadedIds[] = $id;
            wp_set_object_terms($id, ['market-pumps'], 'product_cat');

        }

        // Print Results
        ?>
            <div class="pump-container">


                <div class="pump-row">
                    <?php foreach ($headers as $header) { ?>
                        <span class="pump-box">
                        <?php echo $header ?>
                    </span>
                    <?php } ?>
                </div>
        <?php


        foreach ($uploadedIds as $id) {
            $product = wc_get_product($id);
            $stats = $product->grab_stats();
            ?>

            <div class="pump-row">
                <?php foreach ($headers as $header) { ?>
                    <span class="pump-box"><?php echo isset($stats[$header]) ? $stats[$header] : "" ?></span>
                <?php } ?>
            </div>

            <?php
        }

        ?>
            </div>
        <?php
    }

    /**
     *
     * Takes an image url and attaches it to a product via an attachment
     *
     * @param $image_url
     * @param $post_id
     * @return void
     */
    function Generate_Featured_Image($image_url, $post_id  ): void
    {
        $upload_dir = wp_upload_dir();
        $image_data = file_get_contents($image_url);
        $filename = basename($image_url);
        if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
        else                                    $file = $upload_dir['basedir'] . '/' . $filename;
        file_put_contents($file, $image_data);

        $wp_filetype = wp_check_filetype($filename, null );
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
        $res2= set_post_thumbnail( $post_id, $attach_id );
    }

    /**
     * Wraps a variable in <pre></pre> tags and print_r's it
     *
     * @param $data
     * @return void
     */
    function pprint($data): void
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        echo '<hr>';
    }
}


new lyke_pumper();