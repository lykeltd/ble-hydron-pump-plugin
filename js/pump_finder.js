jQuery(function($) {

    if (!$('.pf-display-container').length) return;

    let params = lyke_url_handler.get_url_params();

    if (params) {

        if (params.has('sku')) {
            let sku = params.get('sku');

            pf_search('sku', sku);

        } else if (params.has('brand')) {
            let brand = params.get('brand');

            pf_search('brand', brand);

        }
    }

    $('#pf-selector').on('change', function(e) {
        e.preventDefault();

        let term = $(this).find(':selected').text();
        let type = $(this).find(':selected').val().split('-')[0];

        pf_search(type, term);
        lyke_url_handler.set_url_params(type, term);
    });



    function pf_search(type, value) {
        console.log('searching');

        // $('#pf-overlay').removeClass('hide');


        $('#pf-display-container').removeClass('expand');
        $('#pf-display-container').addClass('shrink');

        $('#pf-overlay').addClass('pf-overlay');

        let strict = $('#pf-equivalent input').is(':checked');
        console.log(strict);

        // let t = new ldBar('#pf-loading-icon');

        // t.set(0, false);
        // t.set(100, true);

        $.ajax({
            url: lyke.ajaxurl,
            type: 'POST',
            data: {
                action: 'pf_search',
                data: { type: type, value: value, strict: strict }
            },
            success: function (response) {
                // t.set(100, true);

                // $('#pf-overlay').addClass('hide');

                $('.pf-display-container').removeClass('shrink');
                $('.pf-display-container').addClass('expand');

                let reply = response['data'][0]
                $('#pf-display-container').html( reply ? reply: 'No results');
            },
            error: function (response) {
                console.log('Something went wrong');
                console.log(response);
            }
        });
    }

    $('.box').click(function(e) {
        alert('a');
        e.preventDefault();
    });

    function pf_selector_template(option) {
        return option.text;
    }

    $('#pf-selector').select2({ templateResult: pf_selector_template , width: '100%'});
});

// lyke_url_handler = {
//     get_url_params: function () {
//         let url = window.location.search;
//         if (url == '') return false;
//         return new URLSearchParams(url);
//     },
//
//     set_url_params: function (type, value) {
//         let url = new URLSearchParams();
//         url.set(type, value);
//         history.pushState(null, null, "?" + url.toString());
//     },
//
//     set_url_params_custom: function (params) {
//         history.pushState(null, null, "?" + params);
//     }
// }