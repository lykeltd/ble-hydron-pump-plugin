jQuery(function($) {
    $('.nav-item').click(function(e) {
        let id = $(this).attr('id');
        let target = $(this).attr('data-target');

        $(this).closest('.nav').find('.active').removeClass('active');
        $(this).addClass('active');

        if (target === 'selector') {
            $('#selector').removeClass('hide');
            $('#finder').addClass('hide');
        }
        else {
            $('#selector').addClass('hide');
            $('#finder').removeClass('hide');
        }
    });
});