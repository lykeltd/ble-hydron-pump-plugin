jQuery(function($) {

    if (!$('#ps-search-form').length) return;

    let pump_stats = lyke.pump_stats;


    let tags = Object.keys(pump_stats);

    tags.forEach(function (header) {

        pump_stats[header].unshift({
            'value': 'N/A',
            'unit': ''
        });


        $('#ps_' + header + '_slider').slider({
            min: 0,
            max: pump_stats[header].length-1,
            value: 0,
            orientation: "vertical",
            create: function() {
                // $(this).append($('<span></span>').css("left","-10%").addClass("ps-digit-end").text('N/A'));
                for (let i=0; i < pump_stats[header].length; i++)
                {
                    $(this).css('height', pump_stats[header].length * 40 + 'px');
                    let label = $('<span></span>').css("top",((i)*(100/pump_stats[header].length))+"%").addClass("slider-digit").text(pump_stats[header][i]['value']);
                    label.attr('id',header + '_' + i);
                    $(this).append(label);
                }
                // $(this).append($('<span></span>').addClass("ps-digit-end").css("left","100%").text(pump_stats[header][pump_stats[header].length-1]['value']));
            },
            slide: function( event, ui ) {
                $('#ps_' + header + '_result').html(ui.value > 0 ? pump_stats[header][ui.value]['value'] + pump_stats[header][ui.value]['unit'] : 'N/A');


                if (ui.value === 0 || ui.value === pump_stats[header].length) return;


                let activeDigit = $('#' + header + '_' + (ui.value));

                activeDigit.closest('.ps-slider').find('.slider-digit').removeClass('active');

                activeDigit.addClass('active');
            }
        });
    });


    $('[data-slider]').each(function() {
        let min = 0;
        let max = parseInt($(this).attr('data-max'));
        $(this).slider({
            min: min,
            max: max,
            values: [min, max],
            range: true,
            orientation: "horizontal",
            slide: function( event, ui ) {
                $(this).parent().parent().find('.ps-result').text($(this).slider("values")[0] + ' - ' + $(this).slider("values")[1]);
            }

        });
    });


    function get_url_params() {
        let url = window.location.search;
        if (url == '') return false;
        return new URLSearchParams(url);
    }

    function has_url_param(value) {
        let url = new URLSearchParams(window.location.search);
        return url.has(value);

    }

    // if (!has_url_param('q')) { // TO-DO Improve this
    //     let params = get_url_params();
    //     let values = [];
    //
    //     if (params) {
    //
    //         for (let p of params) {
    //
    //             values.push({
    //                 id: p[0],
    //                 value: p[1]
    //             });
    //         }
    //
    //         console.log(params.length, values);
    //         ps_search(values);
    //     }
    // }

    // $('.slider-container').click(function(e) {
    //
    //     let content = $(this).find('.slider-content');
    //
    //     content.slideToggle('fast');
    // });

    $('#ps-back-btn').click(function (e) {
       e.preventDefault();

        history.pushState(null, null, "?");
        location.reload();

    });

    $('#ps_submit_btn').click(function(e) {
        e.preventDefault();

        let values = [];

        let valueElements = $('.ps-result');
        valueElements.each(function () {
            let id = $(this).attr('id');
            let id_split = id.split('ps_');
            id_split = id_split[1].split('_result')[0];
            // values[id] = $(this).text();
            values.push({
                id: id_split,
                value: $(this).text()
            });
        });

        ps_search(values);
    });

    function ps_search(values) {
        $.ajax({
            url: lyke.ajaxurl,
            type: 'POST',
            data: {
                action: 'ps_search',
                args: values
            },
            success: function (response) {

                $('#ps-search-form').addClass('shrink');
                $('#ps-results').html(response['data'][0]);
                $('#ps-results ul').addClass("expand");

                let values_to_keep = {};

                values.forEach(function( value ) {
                    if (value['value'] !== 'N/A') {
                        values_to_keep[value['id']] = value['value'];
                    }
                });

                lyke_url_handler.set_url_params_custom($.param(values_to_keep));

                $('#ps-back-btn').removeClass('hidden');

            },
            error: function(error) {
                console.log(error);
            }
        });
    }
});

lyke_url_handler = {
    get_url_params: function () {
        let url = window.location.search;
        if (url === '') return false;
        return new URLSearchParams(url);
    },

    set_url_params: function (type, value) {
        let url = new URLSearchParams();
        url.set(type, value);
        history.pushState(null, null, "?" + url.toString());
    },

    set_url_params_custom: function (params) {
        history.pushState(null, null, "?" + params);
    }
}

