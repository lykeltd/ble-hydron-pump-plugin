lyke_url_handler = {
    get_url_params: function () {
        let url = window.location.search;
        if (url == '') return false;
        return new URLSearchParams(url);
    },

    set_url_params: function (type, value) {
        let url = new URLSearchParams();
        url.set(type, value);
        history.pushState(null, null, "?" + url.toString());
    },

    set_url_params_custom: function (params) {
        history.pushState(null, null, "?" + params);
    }
}